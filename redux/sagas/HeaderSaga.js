import { put, takeLatest } from 'redux-saga/effects';
import axios from 'axios';
import { INIT_HEADER } from '../types/HeaderTypes';
import { setHeader } from '../actions/HeaderActions';

function* fetchHeaderData() {
  try {
    const request = yield axios.get(`${process.env.API_URL}/top-navigation`);
    yield put(setHeader(request.data));
  } catch (err) {
    console.log('err', err); // TODO console.log for now will change later
  }
}

function* HeaderSaga() {
  yield takeLatest(INIT_HEADER, fetchHeaderData);
}

export default HeaderSaga;
