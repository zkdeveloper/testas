import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import { colors } from '../../theme';

const setPadding = (direction) => {
  if (direction === 'left') {
    return 'padding-left: 15%;';
  }

  return 'padding-right: 15%';
};

const StyledImage = styled.img`
  height: auto;
  ${({ direction }) => setPadding(direction)};
  width 100%;

  @media (max-width: 1024px) {
    padding: 3rem 0 0;
    display: flex;
    justify-content: center;
    margin: 4rem auto 0;
    max-width: 64rem;
    padding: 0;
  }

  @media (max-width: 768px) {
    margin: 3rem auto 0;
  }
`;

const Image = ({ direction, img }) => {
  return (
    <StyledImage direction={direction} src={`https://test.strivr.io${img}`} />
  ); // TODO env file gets error investigate in the future process.browser returns true
};

Image.propTypes = {
  img: PropTypes.string.isRequired,
  direction: PropTypes.string.isRequired,
};

export default Image;
