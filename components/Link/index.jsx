import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import { useSelector } from 'react-redux';
import { colors, fonts } from '../../theme';

export const LinkStyled = styled.a`
  background-color: transparent;
  color: ${colors.$white};
  border: 2px solid ${colors.$white};
  font-size: 1.6rem;
  margin-top: 2.7rem;
  line-height: 2.4rem;
  letter-spacing: normal;
  padding: 1.4rem 0 1rem;
  width: 16rem;
  box-shadow: 0 4px 10px 0 rgba(51, 173, 176, 0.25);
  text-align: center;
  text-decoration: none;
  display: flex;
  justify-content: center;
  align-items: center;
  text-transform: uppercase;
  transition: all 0.2s ease-in;
  cursor: pointer;

  &:hover {
    background-color: ${colors.$white};
    color: ${colors.$greenDarker};
  }

  @media (max-width: 1024px) {
    margin: 2.7rem auto 0;
  }
`;

export const LinkIconStyled = styled.img`
  padding-right: 1rem;
  width: 3rem;
  height: auto;
`;

const Link = ({ href, text, icon }) => {
  return (
    <LinkStyled href={href} target="_blank">
      {icon && <LinkIconStyled src={`https://test.strivr.io${icon}`} />}
      {text}
    </LinkStyled>
  );
};

Link.propTypes = {
  text: PropTypes.string.isRequired,
  href: PropTypes.string.isRequired,
  icon: PropTypes.string.isRequired,
};

export default Link;
