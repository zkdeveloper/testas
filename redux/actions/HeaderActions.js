import { INIT_HEADER, SET_HEADER } from '../types/HeaderTypes';

export const initHeader = (payload) => {
  return {
    type: INIT_HEADER,
    payload,
  };
};

export const setHeader = (payload) => {
  return {
    type: SET_HEADER,
    payload,
  };
};
