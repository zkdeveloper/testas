import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import { colors } from '../../theme';

export const MainContainerStyled = styled.div`
  padding-top: 7.2rem;
`;

const MainContainer = ({ children }) => {
  return <MainContainerStyled>{children}</MainContainerStyled>;
};

MainContainer.propTypes = {
  children: PropTypes.node.isRequired,
};

export default MainContainer;
