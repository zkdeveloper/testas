import React from 'react';
// import PropTypes from 'prop-types';
import styled from 'styled-components';
import { useSelector } from 'react-redux';
import { colors } from '../../theme';

export const BurgerButton = styled.button`
  background-color: ${colors.greenDarker};
  color: #ffffff;
  border-radius: 40px;
  border: 0;
  font-size: 1.5rem;
  letter-spacing: 0.1rem;
  display: block;
  padding: 2rem;
  margin: 3rem auto;
  width: 100%;
  text-align: center;
  cursor: pointer;
  outline: none;

  @media (max-width: 1024px) {
    padding: 2rem;
    width: 100%;
  }

  &:hover {
  }

  &:active {
  }
`;

const Burger = () => {
  return <BurgerButton />;
};

export default Burger;
