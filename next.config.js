require('dotenv').config();
const withFonts = require('next-fonts');

module.exports = withFonts({
  publicRuntimeConfig: {
    API_URL: process.env.API_URL,
    NODE_ENV: process.env.NODE_ENV,
    PORT: process.env.PORT || 3000,
  },
  webpack(config, options) {
    return config;
  },
});
