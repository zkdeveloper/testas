import React from 'react';
// import PropTypes from 'prop-types';
import styled from 'styled-components';
import { useSelector } from 'react-redux';
import { colors } from '../../theme';

import FlexContainer from '../../containers/FlexContainer';

export const StyledFooter = styled.footer`
  background: ${colors.$white};
  width: 100%;
  z-index: 100;
  0px 1px 5px 0px rgba(0, 0, 0, 0.1);
  height: 7.2rem;
`;

const Footer = (props) => {
  const footerData = useSelector((state) => state.FooterReducer.FooterReducer);

  console.log('footerData', footerData, props);

  return (
    <StyledFooter>
      <FlexContainer
        flexDirection={{
          desktop: 'row',
          mobile: 'row',
        }}
        margin="0"
        maxWidth="127rem"
        justifyContent={{
          desktop: 'space-between',
          mobile: 'space-between',
        }}
      />
    </StyledFooter>
  );
};

export default Footer;
