import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import { useSelector } from 'react-redux';
import { fonts } from '../../theme';

export const ParagraphStyled = styled.p`
  font-size: 1.8rem;
  margin-top: 2.1rem;
  max-width: ${({ maxWidth }) => maxWidth};
  line-height: 2.4rem;
  letter-spacing: normal;
  text-align: ${({ align }) => align.desktop};

  @media (max-width: 1024px) {
    margin-top: 1.1rem;
    font-size: 1.6rem;
    line-height: 2.4rem;
    text-align: ${({ align }) => align.mobile};
  }
`;

const Paragraph = ({ align, text, maxWidth }) => {
  return (
    <ParagraphStyled align={align} maxWidth={maxWidth}>
      {text}
    </ParagraphStyled>
  );
};

Paragraph.propTypes = {
  text: PropTypes.string.isRequired,
  maxWidth: PropTypes.string.isRequired,
  align: PropTypes.shape({
    desktop: PropTypes.string.isRequired,
    mobile: PropTypes.string.isRequired,
  }),
};

Paragraph.defaultProps = {
  align: {
    desktop: 'left',
    mobile: 'left',
  },
};

export default Paragraph;
