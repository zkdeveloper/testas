import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import Slider from 'react-slick';
import { colors } from '../../theme';

import Heading from '../Heading';

const StyledCarousel = styled.section`
  color: ${colors.$blackLight};
  padding: 8rem 0 5rem;

  @media (max-width: 1024px) {
    padding: 4rem 0 0;
  }

  .slick-slider {
    margin: 0 auto;
    max-width: 127rem;
    padding: 6.4rem 0 6.4rem;
    overflow: hidden;

    @media (max-width: 1024px) {
      padding: 3.2rem 2.8rem 3.2rem 2.8rem;
    }

    @media (max-width: 768px) {
      padding: 2rem 1.2rem 2rem 1.2rem;
    }
  }

  .slick-list {
    overflow: hidden;
  }

  .slick-track {
    display: flex;
  }

  .slick-slide {
    padding: 2rem 2rem 2rem 2rem;
  }
`;

const StyledCarouselLogo = styled.img`
  width: 30%;
`;

const Carousel = ({ content }) => {
  const settings = {
    dots: false,
    infinite: true,
    slidesToShow: 4,
    slidesToScroll: 1,
    rows: 1,
    autoplay: true,
    speed: 500,
    arrows: false,
    autoplaySpeed: 2000,
    swipe: false,
    swipeToSlide: false,

    responsive: [
      {
        breakpoint: 1024,
        settings: {
          slidesToShow: 3,
        },
      },
      {
        breakpoint: 768,
        settings: {
          slidesToShow: 2,
        },
      },
    ],
  };
  return (
    <StyledCarousel>
      {content.Title && (
        <Heading
          level={2}
          text={content.Title}
          align={{
            desktop: 'center',
            mobile: 'center',
          }}
        />
      )}
      <Slider {...settings}>
        {content?.Logos.map((logo) => (
          <StyledCarouselLogo
            key={logo._id}
            src={`https://test.strivr.io${logo.url}`}
          />
        ))}
      </Slider>
    </StyledCarousel>
  );
};

Carousel.propTypes = {
  content: PropTypes.shape({
    Logos: PropTypes.arrayOf(
      PropTypes.shape({
        alternativeText: PropTypes.string,
        url: PropTypes.string,
      }),
    ),
    Title: PropTypes.string,
  }).isRequired,
};

export default Carousel;
