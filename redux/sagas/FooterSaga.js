import { put, takeLatest } from 'redux-saga/effects';
import axios from 'axios';
import { INIT_FOOTER } from '../types/FooterTypes';
import { setFooter } from '../actions/FooterActions';

function* fetchFooterData() {
  try {
    const request = yield axios.get(`${process.env.API_URL}/footer`);
    console.log('fetchFooterData', request.data);
    yield put(setFooter(request.data));
  } catch (err) {
    console.log('err', err); // TODO console.log for now will change later
  }
}

function* FooterSaga() {
  yield takeLatest(INIT_FOOTER, fetchFooterData);
}

export default FooterSaga;
