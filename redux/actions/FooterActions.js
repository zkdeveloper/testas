import { INIT_FOOTER, SET_FOOTER } from '../types/FooterTypes';

export const initFooter = (payload) => {
  return {
    type: INIT_FOOTER,
    payload,
  };
};

export const setFooter = (payload) => {
  return {
    type: SET_FOOTER,
    payload,
  };
};
