import React from 'react';
import { END } from 'redux-saga';
import { useSelector } from 'react-redux';
import { wrapper } from '../store';
import { initHomePage } from '../redux/actions/HomePageActions';
import { initHeader } from '../redux/actions/HeaderActions';
import { initFooter } from '../redux/actions/FooterActions';

import MainLayout from '../layouts/MainLayout';

const Home = () => {
  const homeState = useSelector(
    (state) => state.HomePageReducer.HomePageReducer, // TODO investigate double nesting issue with combine reducers and wrapper
  );

  return (
    <div className="app">
      <main>
        <MainLayout data={homeState.homeData} isLoading={homeState.isLoading} />
      </main>
    </div>
  );
};

export const getServerSideProps = wrapper.getServerSideProps(async ({ store }) => {
  if (!store.getState().logo || !store.getState().links) {
    store.dispatch(initHeader());
    store.dispatch(initFooter());
  }
  if (!store.getState().data) {
    store.dispatch(initHomePage());
    store.dispatch(END);
  }
  await store.sagaTask.toPromise();
});

export default Home;
