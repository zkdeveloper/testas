import { INIT_HOMEPAGE, SET_HOMEPAGE } from '../types/HomePageTypes';

const initialState = {
  homeData: null,
  isLoading: false,
};

const HomePageReducer = (state = initialState, action) => {
  const { payload, type } = action;
  switch (type) {
    case '__NEXT_REDUX_WRAPPER_HYDRATE__': {
      if (payload.homeData === null) delete payload.homeData;
      return { ...state, ...payload };
    }
    case INIT_HOMEPAGE:
      return {
        ...state,
        ...{ isLoading: true },
      };
    case SET_HOMEPAGE:
      return {
        ...state,
        ...{ isLoading: false },
        ...{ homeData: payload },
      };
    default:
      return state;
  }
};

export default HomePageReducer;
