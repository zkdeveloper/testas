import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import { useSelector } from 'react-redux';
import { colors } from '../../theme';

import Heading from '../Heading';
import Image from '../Image';
import Paragraph from '../Paragraph';
import FlexContainer from '../../containers/FlexContainer';
import Link from '../Link';

const setBackgroundColor = (color) => {
  if (color === 'Green') {
    return colors.$green;
  }

  return colors.$white;
};

const setTextColor = (color) => {
  if (color === 'Green') {
    return colors.$white;
  }

  return colors.$blackLight;
};

const StyledSection = styled.section`
  color: ${({ color }) => setTextColor(color)};
  background-color: ${({ color }) => setBackgroundColor(color)};
  padding: 10rem 4.8rem;
  display: flex;
  justify-content: space-between;
  text-align: left;

  @media (max-width: 1024px) {
    padding: 6.4rem 3.2rem;
  }

  @media (max-width: 768px) {
    padding: 4.8rem 2rem;
  }
`;

const Section = ({ content }) => {
  return (
    <StyledSection color={content.Color}>
      <FlexContainer
        flexDirection={{
          desktop: content?.Image_Left ? 'row-reverse' : 'row',
          mobile: 'column',
        }}
        margin="0 auto"
        maxWidth="127rem"
        justifyContent={{
          desktop: 'space-between',
          mobile: 'center',
        }}
      >
        <FlexContainer
          flexDirection={{
            desktop: 'column',
            mobile: 'column',
          }}
          margin="0"
          maxWidth="auto"
          justifyContent={{
            desktop: 'center',
            mobile: 'center',
          }}
        >
          {content.Title && (
            <Heading
              level={content?.Title_Level ? content.Title_Level : 2}
              text={content.Title}
              align={{
                desktop: content.Image?.url ? 'left' : 'center',
                mobile: 'center',
              }}
            />
          )}
          {content.Description && (
            <Paragraph
              maxWidth="100%"
              text={content.Description}
              align={{
                desktop: content.Image?.url ? 'left' : 'center',
                mobile: 'center',
              }}
            />
          )}
          {content.Button && content.Button_Link && (
            <Link
              text={content.Button}
              href={content.Button_Link}
              icon={content.Button_Icon?.url}
            />
          )}
        </FlexContainer>
        {content.Image && (
          <div>
            <Image
              direction={content?.Image_Left ? 'right' : 'left'}
              img={content.Image?.url}
            />
          </div>
        )}
      </FlexContainer>
    </StyledSection>
  );
};

Section.propTypes = {
  content: PropTypes.shape({
    Button: PropTypes.string,
    Button_Link: PropTypes.string,
    Button_Icon: PropTypes.shape({
      alternativeText: PropTypes.string,
      url: PropTypes.string,
    }),
    Color: PropTypes.string,
    Description: PropTypes.string,
    id: PropTypes.string,
    Image: PropTypes.shape({
      alternativeText: PropTypes.string,
      url: PropTypes.string,
    }),
    Title_Level: PropTypes.number,
    Title: PropTypes.string,
    Image_Left: PropTypes.bool,
  }).isRequired,
};

export default Section;
