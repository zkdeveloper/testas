import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import { useSelector } from 'react-redux';
import { fonts } from '../../theme';

const setFontSize = (level, number) => {
  const maximum = 3.6;

  return `${maximum - Number(level) * number}rem`;
};

export const HeadingStyled = styled.h1`
  text-align: ${({ align }) => align.desktop};
  font-size: ${({ level }) => setFontSize(level, 0.2)};
  font-weight: ${fonts.$bold};
  line-height: normal;
  letter-spacing: normal;

  @media (max-width: 1024px) {
    text-align: ${({ align }) => align.mobile};
    font-size: ${({ level }) => setFontSize(level, 0.4)};
  }
`;

const Heading = ({ align, level, text }) => {
  return (
    <HeadingStyled as={`h${level.toString()}`} level={level} align={align}>
      {text}
    </HeadingStyled>
  );
};

Heading.propTypes = {
  align: PropTypes.shape({
    desktop: PropTypes.string.isRequired,
    mobile: PropTypes.string.isRequired,
  }),
  level: PropTypes.oneOf([1, 2, 3, 4, 5, 6]).isRequired,
  text: PropTypes.string.isRequired,
};

Heading.defaultProps = {
  align: {
    desktop: 'center',
    mobile: 'center',
  },
};

export default Heading;
