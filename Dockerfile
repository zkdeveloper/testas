FROM node:10-stretch-slim AS build
WORKDIR /app
COPY . ./
RUN npm install --only=production --no-audit && npm run build

FROM node:10-stretch-slim
WORKDIR /app
COPY --from=build /app /app
CMD ["npm", "start"]
