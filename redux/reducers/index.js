import { combineReducers } from 'redux';

import HeaderReducer from './HeaderReducer';
import FooterReducer from './FooterReducer';
import HomePageReducer from './HomePageReducer';

const root = combineReducers({
  HeaderReducer,
  FooterReducer,
  HomePageReducer,
});

export default root;
