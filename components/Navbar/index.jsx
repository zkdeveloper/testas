import React from 'react';
// import PropTypes from 'prop-types';
import styled from 'styled-components';
import { useSelector } from 'react-redux';
import { colors } from '../../theme';

import FlexContainer from '../../containers/FlexContainer';

export const StyledNavbar = styled.header`
  position: fixed;
  background: ${colors.$white};
  width: 100%;
  z-index: 100;
  box-shadow: 0px 1px 5px 0px rgba(0, 0, 0, 0.1);
  height: 7.2rem;
  display: flex;
  align-items: center;
  padding: 0 6.4rem;

  @media (max-width: 1024px) {
    padding: 0 4.8rem;
  }

  @media (max-width: 768px) {
    padding: 0 3.2rem;
  }
`;

export const StyledNavbarLogo = styled.img`
  padding-right: 1rem;
  width: 10rem;
  height: auto;
`;

const Navbar = (props) => {
  const headerLogo = useSelector(
    (state) => state.HeaderReducer.HeaderReducer.logo,
  );

  return (
    <StyledNavbar>
      <FlexContainer
        flexDirection={{
          desktop: 'row',
          mobile: 'row',
        }}
        margin="0"
        maxWidth="127rem"
        justifyContent={{
          desktop: 'space-between',
          mobile: 'space-between',
        }}
      >
        <StyledNavbarLogo src={`https://test.strivr.io${headerLogo?.url}`} />
      </FlexContainer>
    </StyledNavbar>
  );
};

export default Navbar;
