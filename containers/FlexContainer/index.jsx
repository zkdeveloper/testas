import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import { colors } from '../../theme';

export const FlexContainerStyled = styled.div`
  display: flex;
  flex-direction: ${({ flexDirection }) => flexDirection.desktop};
  max-width: ${({ maxWidth }) => maxWidth};
  margin: ${({ margin }) => margin};
  justify-content: ${({ justifyContent }) => justifyContent.desktop};

  @media (max-width: 1024px) {
    flex-direction: ${({ flexDirection }) => flexDirection.mobile};
    justify-content: ${({ justifyContent }) => justifyContent.mobile};
  }
`;

const FlexContainer = ({
  children,
  flexDirection,
  justifyContent,
  maxWidth,
  margin,
}) => {
  return (
    <FlexContainerStyled
      margin={margin}
      maxWidth={maxWidth}
      flexDirection={flexDirection}
      justifyContent={justifyContent}
    >
      {children}
    </FlexContainerStyled>
  );
};

FlexContainer.propTypes = {
  children: PropTypes.node.isRequired,
  flexDirection: PropTypes.shape({
    desktop: PropTypes.string.isRequired,
    mobile: PropTypes.string.isRequired,
  }).isRequired,
  justifyContent: PropTypes.shape({
    desktop: PropTypes.string.isRequired,
    mobile: PropTypes.string.isRequired,
  }).isRequired,
  maxWidth: PropTypes.string.isRequired,
  margin: PropTypes.string.isRequired,
};

export default FlexContainer;
