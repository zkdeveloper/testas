import { INIT_HEADER, SET_HEADER } from '../types/HeaderTypes';

const initialState = {
  logo: null,
  links: null,
};

const HeaderReducer = (state = initialState, action) => {
  const { payload, type } = action;
  switch (type) {
    case '__NEXT_REDUX_WRAPPER_HYDRATE__': {
      if (payload.logo === null) delete payload.logo;
      return { ...state, ...payload };
    }
    case INIT_HEADER:
      return {
        ...state,
      };
    case SET_HEADER:
      return {
        ...state,
        ...{ logo: payload.Logo },
        // ...{ links: payload.Logo },
      };
    default:
      return state;
  }
};

export default HeaderReducer;
