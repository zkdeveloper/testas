import { INIT_HOMEPAGE, SET_HOMEPAGE } from '../types/HomePageTypes';

export const initHomePage = (payload) => {
  console.log('initHomePage', payload);
  return {
    type: INIT_HOMEPAGE,
    payload,
  };
};

export const setHomePage = (payload) => {
  return {
    type: SET_HOMEPAGE,
    payload,
  };
};
