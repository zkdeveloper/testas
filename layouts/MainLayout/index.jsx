import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import { useSelector } from 'react-redux';
import Reveal from 'react-reveal/Reveal';
import { colors } from '../../theme';

import { SECTION, CAROUSEL, TESTIMONIAL } from '../../constants';

import Section from '../../components/Section';
import Carousel from '../../components/Carousel';
import Testimonial from '../../components/Testimonial';
import Container from '../../containers/MainContainer';
import Navbar from '../../components/Navbar';
import Footer from '../../components/Footer';

export const MainLayoutStyled = styled.div``;

const MainLayout = ({ data = [], isLoading }) => {
  const getContent = (content) => {
    switch (true) {
      case content.__component.includes(SECTION):
        return (
          <Reveal>
            <Section key={content.id} content={content} />
          </Reveal>
        );
      case content.__component.includes(CAROUSEL):
        return (
          <Reveal>
            <Carousel key={content.id} content={content} />
          </Reveal>
        );
      case content.__component.includes(TESTIMONIAL):
        return (
          <Reveal>
            <Testimonial key={content.id} content={content} />
          </Reveal>
        );
      default:
        return null;
    }
  };

  return (
    <MainLayoutStyled>
      <Navbar />
      <Container>
        {data &&
          data.Content.length > 0 &&
          data.Content.map((content) => getContent(content))}
      </Container>
      <Footer />
    </MainLayoutStyled>
  );
};

MainLayout.propTypes = {
  data: PropTypes.shape({
    Content: PropTypes.array.isRequired,
  }).isRequired,
  isLoading: PropTypes.bool.isRequired,
};

export default MainLayout;
