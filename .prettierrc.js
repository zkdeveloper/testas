module.exports = {
  semi: true,
  trailingComma: "all",
  singleQuote: true,
  // printWidth: 100,
  indent_size: 2
};
