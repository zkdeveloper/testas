import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import Slider from 'react-slick';
import { colors } from '../../theme';

import Heading from '../Heading';
import Paragraph from '../Paragraph';
import FlexContainer from '../../containers/FlexContainer';

const StyledTestimonial = styled.section`
  color: ${colors.$blackLight};
  padding: 4.8rem 0 2rem;

  .slick-slider {
    margin: 0 auto;
    max-width: 127rem;
    padding: 6.4rem 4.4rem 6.4rem 4.4rem;
    overflow: hidden;

    @media (max-width: 1024px) {
      padding: 6.4rem 2.8rem 6.4rem 2.8rem;
    }

    @media (max-width: 768px) {
      padding: 6.4rem 1.2rem 6.4rem 1.2rem;
    }
  }

  .slick-list {
    overflow: hidden;
  }

  .slick-track {
    display: flex;
  }

  .slick-slide {
    padding: 2rem 2rem 2rem 2rem;
  }
`;

const StyledTestimonialLogo = styled.img`
  width: 30%;
  margin-right: 5rem;
`;

const Testimonial = ({ content }) => {
  console.log('Testimonial', content);
  const settings = {
    dots: false,
    infinite: true,
    slidesToShow: 1,
    slidesToScroll: 1,
    rows: 1,
    autoplay: true,
    speed: 500,
    arrows: false,
    autoplaySpeed: 2000,
    swipe: false,
    swipeToSlide: false,

    responsive: [
      {
        breakpoint: 1024,
        settings: {
          slidesToShow: 1,
        },
      },
      {
        breakpoint: 768,
        settings: {
          slidesToShow: 1,
        },
      },
    ],
  };
  return (
    <StyledTestimonial>
      <Slider {...settings}>
        {content?.Content.map((item) => (
          <FlexContainer
            key={item._id}
            flexDirection={{
              desktop: 'row',
              mobile: 'column',
            }}
            margin="0 auto"
            maxWidth="90rem"
            justifyContent={{
              desktop: 'space-between',
              mobile: 'center',
            }}
          >
            <StyledTestimonialLogo
              src={`https://test.strivr.io${item?.Logo?.url}`}
            />
            <Paragraph maxWidth="50%" text={item?.Text} />
          </FlexContainer>
        ))}
      </Slider>
    </StyledTestimonial>
  );
};

Testimonial.propTypes = {
  content: PropTypes.shape({
    Content: PropTypes.arrayOf(
      PropTypes.shape({
        Title: PropTypes.string,
        Logo: PropTypes.shape({
          alternativeText: PropTypes.string,
          url: PropTypes.string,
        }),
      }),
    ),
  }).isRequired,
};

export default Testimonial;
