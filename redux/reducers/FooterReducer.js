import { INIT_FOOTER, SET_FOOTER } from '../types/FooterTypes';

const initialState = {
  phone: null,
  email: null,
  copyright: '',
  addresses: null,
};

const FooterReducer = (state = initialState, action) => {
  const { payload, type } = action;
  switch (type) {
    case '__NEXT_REDUX_WRAPPER_HYDRATE__': {
      return { ...state, ...payload };
    }
    case INIT_FOOTER:
      return {
        ...state,
      };
    case SET_FOOTER:
      return {
        ...state,
        ...{ phone: payload.Phone },
        ...{ email: payload.Email },
        ...{ copyright: payload.Copyright },
        // ...{ addresses: payload.addresses },
      };
    default:
      return state;
  }
};

export default FooterReducer;
