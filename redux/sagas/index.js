import { fork } from 'redux-saga/effects';

import HeaderSaga from './HeaderSaga';
import FooterSaga from './FooterSaga';
import HomePageSaga from './HomePageSaga';

export default function* rootSaga() {
  yield fork(HeaderSaga);
  yield fork(FooterSaga);
  yield fork(HomePageSaga);
}
