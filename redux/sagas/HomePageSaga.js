import { put, takeLatest } from 'redux-saga/effects';
import axios from 'axios';
import { INIT_HOMEPAGE } from '../types/HomePageTypes';
import { setHomePage } from '../actions/HomePageActions';

function* fetchHomePageData() {
  try {
    const request = yield axios.get(`${process.env.API_URL}/home-page`);
    yield put(setHomePage(request.data));
  } catch (err) {
    console.log('err', err); // TODO console.log for now will change later
  }
}

function* HomePageSaga() {
  yield takeLatest(INIT_HOMEPAGE, fetchHomePageData);
}

export default HomePageSaga;
