import { createGlobalStyle } from 'styled-components';

export const colors = {
  $black: '#000',
  $white: '#ffffff',
  $blackLight: '#4d4d4d',
  $grayLight: '#f5f5f5',
  $gray: '#d4d4d4',
  $grayDarker: '#c4c4c4',
  $grayDark: '#a4a4a4',
  $greenLight: '#d7ebe0',
  $greenLighter: '#eaf1ed',
  $green: '#45ac76',
  $greenDarker: '#307b54',
  $greenDarkest: '#3f8959',
  $yellow: '#fb9f00',
};

export const fonts = {
  $fontFamily: 'Lota Grotesque, sans-serif',
  $thin: '100',
  $extraLight: '200',
  $light: '300',
  $normal: '400',
  $semiBold: '600',
  $bold: '700',
  $heavy: '900',
  $regular: 'normal',
  $inherit: 'inherit',
};

export const main = {};

export const GlobalStyle = createGlobalStyle`
  @font-face {
    font-family: 'Lota Grotesque';
    src: url('./fonts/los_andes_-_lota_grotesque_regular-webfont.woff2') format('woff2'),
    url('./fonts/los_andes_-_lota_grotesque_regular-webfont.woff') format('woff'),
    url('./fonts/los_andes_-_lota_grotesque_regular-webfont.ttf') format('ttf');
    font-weight: ${fonts.$regular};
    font-style: normal;
  }

  @font-face {
    font-family: 'Lota Grotesque';
    src: url('./fonts/los_andes_-_lota_grotesque_regular_italic-webfont.woff2') format('woff2'),
    url('./fonts/los_andes_-_lota_grotesque_regular_italic-webfont.woff') format('woff');
    font-weight: ${fonts.$regular};
    font-style: italic;
  }

  @font-face {
    font-family: 'Lota Grotesque';
    src: url('../fonts/los_andes_-_lota_grotesque_semi_bold-webfont.woff2') format('woff2'),
    url('./fonts/los_andes_-_lota_grotesque_semi_bold-webfont.woff') format('woff');
    font-weight: ${fonts.$semiBold};
    font-style: normal;
  }

  @font-face {
    font-family: 'Lota Grotesque';
    src: url('./fonts/los_andes_-_lota_grotesque_semi_bold_italic-webfont.woff2') format('woff2'),
    url('./fonts/los_andes_-_lota_grotesque_semi_bold_italic-webfont.woff') format('woff');
    font-weight: ${fonts.$semiBold};
    font-style: italic;
  }

  @font-face {
    font-family: 'Lota Grotesque';
    src: url('../fonts/los_andes_-_lota_grotesque_thin-webfont.woff2') format('woff2'),
    url('./fonts/los_andes_-_lota_grotesque_thin-webfont.woff') format('woff');
    font-weight: ${fonts.$thin};
    font-style: normal;
  }

  @font-face {
    font-family: 'Lota Grotesque';
    src: url('./fonts/los_andes_-_lota_grotesque_thin_italic-webfont.woff2') format('woff2'),
    url('./fonts/los_andes_-_lota_grotesque_thin_italic-webfont.woff') format('woff'),
    url('./fonts/los_andes_-_lota_grotesque_thin_italic-webfont.ttf') format('ttf');
    font-weight: ${fonts.$thin};
    font-style: italic;
  }

  @font-face {
    font-family: 'Lota Grotesque';
    src: url('./fonts/los_andes_-_lota_grotesque_light-webfont.woff2') format('woff2'),
    url('./fonts/los_andes_-_lota_grotesque_light-webfont.woff') format('woff'),
    url('./fonts/los_andes_-_lota_grotesque_light-webfont.ttf') format('ttf');
    font-weight: ${fonts.$light};
    font-style: normal;
  }

  @font-face {
    font-family: 'Lota Grotesque';
    src: url('./fonts/los_andes_-_lota_grotesque_light_italic-webfont.woff2') format('woff2'),
    url('./fonts/los_andes_-_lota_grotesque_light_italic-webfont.woff') format('woff');
    font-weight: ${fonts.$light};
    font-style: italic;
  }

  @font-face {
    font-family: 'Lota Grotesque';
    src: url('./fonts/los_andes_-_lota_grotesque_extra_light-webfont.woff2') format('woff2'),
    url('./fonts/los_andes_-_lota_grotesque_extra_light-webfont.woff') format('woff'),
    url('./fonts/los_andes_-_lota_grotesque_extra_light-webfont.ttf') format('ttf');
    font-weight: ${fonts.$extraLight};
    font-style: normal;
  }

  @font-face {
    font-family: 'Lota Grotesque';
    src: url('./fonts/los_andes_-_lota_grotesque_extra_light_italic-webfont.woff2') format('woff2'),
    url('./fonts/los_andes_-_lota_grotesque_extra_light_italic-webfont.woff') format('woff'),
    url('./fonts/los_andes_-_lota_grotesque_extra_light_italic-webfont.ttf') format('ttf');
    font-weight: ${fonts.$extraLight};
    font-style: italic;
  }

  @font-face {
    font-family: 'Lota Grotesque';
    src: url('./fonts/los_andes_-_lota_grotesque_bold-webfont.woff2') format('woff2'),
    url('./fonts/los_andes_-_lota_grotesque_bold-webfont.woff') format('woff');
    font-weight: ${fonts.$bold};
    font-style: normal;
  }

  @font-face {
    font-family: 'Lota Grotesque';
    src: url('./fonts/los_andes_-_lota_grotesque_bold_italic-webfont.woff2') format('woff2'),
    url('./fonts/los_andes_-_lota_grotesque_bold_italic-webfont.woff') format('woff');
    font-weight: ${fonts.$bold};
    font-style: italic;
  }

  @font-face {
    font-family: 'Lota Grotesque';
    src: url('./fonts/los_andes_-_lota_grotesque_black-webfont.woff2') format('woff2'),
    url('./fonts/los_andes_-_lota_grotesque_black-webfont.woff') format('woff');
    font-weight: ${fonts.$heavy};
    font-style: normal;
  }

  @font-face {
    font-family: 'Lota Grotesque';
    src: url('./fonts/los_andes_-_lota_grotesque_black_italic-webfont.woff2') format('woff2'),
    url('./fonts/los_andes_-_lota_grotesque_black_italic-webfont.woff') format('woff');
    font-weight: ${fonts.$heavy};
    font-style: italic;
  }

  * {
    box-sizing: border-box;
    margin: 0px;
    padding: 0px;
  }

  html,
  button,
  input,
  select,
  textarea {
    color: #FFFFFF;

    &:focus {
      outline: none;
    }
  }

  html {
    font-size: 62.5%;
    height: 100%;
    line-height: 1.4;
    -webkit-font-smoothing: antialiased;
    -moz-osx-font-smoothing: grayscale;
    -webkit-overflow-scrolling: touch;
  }

  body {
    background-color: #FFFFFF;
    height: 100%;
    font-family: ${fonts.$fontFamily};
    margin: 0;
    position: relative;
    scroll-behavior: smooth;
    min-height: 100vh;
    width: 100%;
  }

  .app {
    position: relative;
    z-index: 10;
  }

  audio,
  canvas,
  img,
  video {
    vertical-align: middle;
  }

  fieldset {
    border: 0;
    margin: 0;
    padding: 0;
  }

  textarea {
    resize: vertical;
  }
`;
